import Config from './config';
import React, { createFactory } from 'react';

export default class BaseBuilder {

  getConfig() {
    return Config.components[this.component];
  }

  build() {

  }

}
