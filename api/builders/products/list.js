import Config from '../config';
import BaseBuilder from '../base';
import ProductItemBuilder from './item';

export default class ProductListBuilder extends BaseBuilder {

  constructor(data) {
    super();
    this.component = 'ProductListComponent';
    this.data = data;

    this.props = {
      component: this.component,
      config: {},
      children: []
    };
  }

  build() {
    let config = this.getConfig().properties;

    this.props.children = _.map(this.data, (product) => {
      let builder = new ProductItemBuilder(_.extend({}, product, {
        grid: config.itemsInRow
      }));
      builder.build();

      return builder.props;
    });
  }
}
