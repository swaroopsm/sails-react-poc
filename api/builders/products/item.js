import BaseBuilder from '../base';
import PriceBuilder from '../price';
import ImageBuilder from '../image';

export default class ProductItemBuilder extends BaseBuilder {

  constructor(data) {
    super();
    this.component = 'ProductItemComponent';
    this.data = data;
    this.props = {
      component: this.component,
      config: {},
      children: []
    };
  }

  build() {
    let config = this.getConfig(),
        { component } = this,
        { price } = this.data;

    this.props.config = _.extend({}, _.pick(this.data, 'name', 'condition'));

    if(this.data.grid) {
      this.props.config.grid = 'col-md-' + this.data.grid;
    }

    // Image Builder
    if(this.data.imageLink) {
      let imageBuilder = new ImageBuilder({
        url: this.data.imageLink
      });

      imageBuilder.build();
      this.props.children.push(imageBuilder.props);
    }

    // Price Builder
    if(price) {
      let priceBuilder = new PriceBuilder({
        value: price,
        extraDiscount: this.data.extraDiscount
      });

      priceBuilder.build();
      this.props.children.push(priceBuilder.props);
    }
  }
}
