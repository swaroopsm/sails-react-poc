const config = {
  components: {
    ProductListComponent: {
      builder: 'ProductListBuilder',
      properties: {
        itemsInRow: 3
      }
    },
    ProductItemComponent: {
      builder: 'ProductItemBuilder',
      key: 'product',
      properties: {

      }
    },
    PriceComponent: {
      builder: 'PriceBuilder',
      key: 'price',
      properties: {
        prefix: '$'
      }
    },
    DiscountComponent: {
      properties: {
        suffix: '% Off'
      }
    },
    PriceDiscountIndicatorComponent: {
      properties: {
        priceSlashStyle: 'strikethrough',
        tags: 'roundBrackets'
      }
    },
    ProductSlashedPriceComponent: {
      properties: {
        priceSlashStyle: 'strikethrough'
      }
    },
    ProductWishlistIcon: {
      properties: {
        position: 'topRight'
      }
    },
    ProductCardRibbonComponent: {
      properties: {
        position: 'topLeft'
      }
    },
    ProducatExtraDiscountComponent: {
      properties: {
        position: 'bottomRight',
        text: 'Extra'
      }
    }
  }
};

export default config;
