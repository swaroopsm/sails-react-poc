var Hello = require('../../assets/js/components/hello');

var allowedProperties = {
  includeHeading: {
    type: 'boolean'
  },

};

module.exports = {
  build: function(config) {
    var props = config.properties;

    return {
      getComponent: function() {
        return Hello.hello;
      },

      config: {
        isHeadingAllowed: function() {
          return props.includeHeading;
        }
      }
    }
  }
};
