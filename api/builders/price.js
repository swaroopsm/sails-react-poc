import BaseBuilder from './base';

export default class PriceBuilder extends BaseBuilder {
  constructor(config) {
    super();
    this.component = 'PriceComponent';
    this.config = config;
    this.value = config.value;
    this.props = {
      component: this.component,
      config: {},
      children: []
    };
  }

  getValue() {
    let disc = parseInt(this.config.extraDiscount, 10);

    if(disc && disc >= 20) {
      return this.value
    }

    return null;
  }

  build() {
    let { prefix } = this.getConfig().properties;

    this.props.config = {
      prefix,
      value: this.getValue()
    }
  }
}
