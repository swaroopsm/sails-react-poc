import BaseBuilder from './base';

export default class ImageBuilder extends BaseBuilder {
  constructor(config) {
    super();
    this.component = 'ImageComponent';
    this.config = config;

    this.props = {
      component: this.component,
      config: {},
      children: []
    };
  }

  getValue() {
    let disc = parseInt(this.config.extraDiscount, 10);

    if(disc && disc >= 20) {
      return this.value
    }

    return null;
  }

  build() {
    this.props.config = {
      url: this.config.url
    }
  }
}
