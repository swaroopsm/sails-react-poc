import React from 'react';
import { renderToString } from 'react-dom/server';
import Product from '../../assets/js/components/product';
import ProductListComponent from '../../assets/js/components/products/list';
var Config = require('./config');
var Builders = {};
var Components = {};

Builders = {
  PriceBuilder: require('./price'),
  ProductBuilder: require('./product')
};

Components = {
  ProductComponent: require('../../assets/js/components/product').default
};

module.exports = {

  __config: null,

  getConfig: function() {
    return this.__config;
  },

  init: function() {
    var components = Config.components,
        component,
        builder,
        config = Config,
        result = {};

    for(var key in components) {
      component = components[key];
      builder = Builders[component.builder];

      if(builder) {
        result[key] = builder.build(component.properties);
      }
    }

    console.log('--> Builder loaded');

    this.__config = result;
  },

  getProps: function(data) {
    var config = this.getConfig(),
        children = data.children;

    var props = _.extend({}, config[data.component], data.data)
    if(children && children.length > 0) {
      for(var i=0, length=children.length; i<length; i++) {
        props[Config.components[children[i].component].key] = this.getProps(children[i]);
      }
    }

    return props;
  },

  render: function(props) {
    console.log(props)
    var component = React.createFactory(ProductListComponent);

    return renderToString(component(props));
  }
};
