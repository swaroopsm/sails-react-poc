/**
 * HelloController
 *
 * @description :: Server-side logic for managing helloes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Builder = require('../builders/builder');
var ProductListBuilder = require('../builders/products/list').default;
import { renderToString } from 'react-dom/server';
import { renderTree } from '../../assets/js/renderer';
import React from 'react';
var Components = require('../../assets/js/components/components');

// TODO
// Obtained from a service
var data = [
    {
      id: 8914,
      name: 'Jasper Coffee Table with Stool',
      condition: 'Unboxed',
      price: '26000',
      offerPrice: '13446',
      discount: '49',
      imageLink: 'https://img1.gozefo.com/p/2/7/2/4/2/6/272426-category_Image.jpg',
      extraDiscount: '21'
    },
    {
      id: 8913,
      name: 'Jasper Coffee Table with Stool',
      condition: 'Gently Used',
      price: '15000',
      offerPrice: '7999',
      discount: '47',
      imageLink: 'https://img1.gozefo.com/p/2/9/0/8/3/8/290838-category_Image.jpg',
      extraDiscount: '11'
    }
];

var data2 = [
    {
      id: 8414,
      name: 'Blaine 3-Seater Leather Sofa by Godrej',
      condition: 'Gently Used',
      price: '26000',
      offerPrice: '13446',
      discount: '49',
      imageLink: 'https://img3.gozefo.com/p/3/3/3/8/0/0/333800-category_Image.jpg',
      extraDiscount: '50'
    },
    {
      id: 6913,
      name: 'Cathi King Size Bed with Sidetable',
      condition: 'Well Used',
      price: '10000',
      offerPrice: '7999',
      discount: '47',
      imageLink: 'https://img2.gozefo.com/p/2/5/5/8/5/7/255857-category_Image.jpg',
      extraDiscount: '30'
    }
];

module.exports = {
  index: function(req, res) {
    var list = new ProductListBuilder(data);

    list.build();

    // console.log(list.renderTree());
    // var props = Builder.getProps(data),
    // console.log(JSON.stringify(list.props));
    var el = renderToString(renderTree(list.props, Components));

    return res.view('hello', { el: el, props: list.props });
  },

  load: function(req, res) {
    var list = new ProductListBuilder(data2);
    list.build();

    return res.json(list.props.children);
  }
};

