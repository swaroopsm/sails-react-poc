module.exports = {
  entry: "./assets/entry.js",
  output: {
    path: __dirname,
    filename: "./assets/js/bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: '/node_modules/'
      }
    ]
  }
};
