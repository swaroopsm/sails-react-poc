import _ from 'underscore';
import React from 'react';
import { App as ProductListComponent } from './components/products/list';
import { App as ProductItemComponent } from './components/products/item';
import { App as PriceComponent } from './components/price';
import { App as ImageComponent } from './components/image';

const Components = {
  ProductListComponent,
  ProductItemComponent,
  PriceComponent,
  ImageComponent
};

const constructTree = function constructTree(children) {
  let elements = [];

  if(children && children.length > 0) {
    _.each(children, (child, index) => {
      let { component, config } = child;
      if(component) {
        elements.push(React.createElement(Components[component](), _.extend(config, { component: component, key: index, children: child.children })));
      }
    });
  }

  return elements.length > 0 ? elements : null;
};

export const renderChildren = function(children) {
  return constructTree(children);
};


export const renderTree = function(props) {
  let { component, config } = props;

  return React.createElement(Components[component](), _.extend({ component: component }, config, { children: props.children }));
}
