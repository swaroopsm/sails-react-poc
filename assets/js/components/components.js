var components = {
  ProductListComponent: require('./products/list'),
  ProductItemComponent: require('./products/item'),
  PriceComponent: require('./price'),
  ImageComponent: require('./image')
}

module.exports = components;
