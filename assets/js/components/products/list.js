import React, { Component } from 'react';
import ProductItem from './item';
import { renderChildren } from '../../renderer';

class ProductListComponent extends Component {

  constructor(props) {
    super(props);

    // State
    this.state = {
      products: []
    }

    // Events
    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  handleLoadMore() {
    $.ajax({
      method: 'GET',
      url: '/load',
      success: function(data) {
        this.setState({ products: this.state.products.concat(data) })
      }.bind(this)
    })
  }

  componentWillMount() {
    this.setState({ products: this.props.children });
  }

  render() {
    return (
      <div>
        <div className='row'>
          { renderChildren(this.state.products) }
        </div>

        <button onClick={ this.handleLoadMore }>Load More</button>
      </div>
    );
  }
}

export function App() {
  return ProductListComponent;
}

