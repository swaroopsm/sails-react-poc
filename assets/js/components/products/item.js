import React, { Component } from 'react';
import { renderChildren } from '../../renderer';

export class ProductItemComponent extends Component {

  render() {
    let { name, condition, price } = this.props;

    return (
      <div className={ this.props.grid }>
        <h4>{ name }</h4>
        <p>{ condition }</p>

        { renderChildren(this.props.children) }
      </div>
    );
  }
}

ProductItemComponent.propTypes = {
  grid: React.PropTypes.string,
  name: React.PropTypes.string,
  condition: React.PropTypes.string,
  price: React.PropTypes.object
};

export function App() {
  return ProductItemComponent;
}

