import React, { Component } from 'react';

export class ImageComponent extends Component {

  render() {
    return (
      <img src={ this.props.url } alt="" />
    );
  }
}

export function App() {
  return ImageComponent;
}
