'use strict';

import React, { Component } from 'react';
import Price from './price';

export default class Product extends Component {

  render() {
    let { name, price } = this.props;

    return (
      <div>
        <h1>{ name }</h1>
        <Price {...price} />
      </div>
    );
  }
}
