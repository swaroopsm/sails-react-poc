import React, { Component } from 'react';

export class PriceComponent extends Component {

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  renderPrice() {
    return [ this.props.prefix, this.props.value ].join(' ');
  }


  handleClick() {
    alert(this.renderPrice());
  }

  render() {
    if(!this.props.value) { return false; }

    return (
      <p>
        <a onClick={ this.handleClick }>{ this.renderPrice() }</a>
      </p>
    );
  }
}

export function App() {
  return PriceComponent;
}
