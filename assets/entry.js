import React from 'react';
import { render } from 'react-dom';
import BaseBuilder from '../api/builders/base';
import { renderTree } from './js/renderer';
var Components = require('./js/components/components');

let props = JSON.parse(window.__props__);
console.log(Components)

render(renderTree(props, Components), document.getElementById('container'));
